\select@language {spanish}
\contentsline {section}{\numberline {1}Ejercicios Clase}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Ejercicio 1}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Ejercicio 3 - Nota programaci\IeC {\'o}n}{3}{subsection.1.2}
\contentsline {subsubsection}{\numberline {1.2.1}Tabla de la verdad}{3}{subsubsection.1.2.1}
\contentsline {subsection}{\numberline {1.3}Ejercicio 4 - Mili}{4}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Ejercicio 5 - Seguro}{5}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}Ejercicio 6 - Venta de art\IeC {\'\i }culos}{5}{subsection.1.5}
\contentsline {subsection}{\numberline {1.6}Ejercicio 7 - Promoci\IeC {\'o}n de curso}{6}{subsection.1.6}
\contentsline {subsection}{\numberline {1.7}Ejercicio 8 - Envases}{8}{subsection.1.7}
\contentsline {subsection}{\numberline {1.8}Ejercicio 9 - Previsi\IeC {\'o}n del tiempo}{8}{subsection.1.8}
\contentsline {subsection}{\numberline {1.9}Ejercicio 10 - Bolsa}{10}{subsection.1.9}
\contentsline {subsubsection}{\numberline {1.9.1}Versi\IeC {\'o}n Alfredo (MAL)}{10}{subsubsection.1.9.1}
\contentsline {subsubsection}{\numberline {1.9.2}Versi\IeC {\'o}n M.J. \IeC {\textquestiondown }BIEN?}{11}{subsubsection.1.9.2}
\contentsline {subsection}{\numberline {1.10}Ejercicio 11 - N\IeC {\'o}minas empleados}{12}{subsection.1.10}
\contentsline {subsection}{\numberline {1.11}Ejercicio 12 - Facturaci\IeC {\'o}n clientes}{12}{subsection.1.11}
\contentsline {subsection}{\numberline {1.12}Ejercicio 13 - Productos de cr\IeC {\'e}dito de un banco}{13}{subsection.1.12}
\contentsline {subsection}{\numberline {1.13}Ejercicio 14 - Entrega de libros (a\IeC {\'e}reo/terrestre)}{14}{subsection.1.13}
\contentsline {subsection}{\numberline {1.14}Ejercicio 15 - Modificaci\IeC {\'o}n entrega de libros (a\IeC {\'e}reo/terrestre)}{14}{subsection.1.14}
\contentsline {subsection}{\numberline {1.15}Ejercicio 16 - M\IeC {\'o}dulos de una asignatura}{15}{subsection.1.15}
\contentsline {section}{\numberline {2}Ejercicios Ex\IeC {\'a}menes Pasados}{16}{section.2}
\contentsline {subsection}{\numberline {2.1}Examen Tema 1 2018/2019}{16}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Ejercicio 1}{16}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Ejercicio 2}{18}{subsubsection.2.1.2}
