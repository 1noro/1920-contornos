package bol1ex1;

import java.util.Scanner;

public class Bol1ex1 {
	
	public static String ex1(int a, int b) {
		String out = "";
		if (a == b) {
			out = "IGUALES";
		} else if (a % b == 0) {
			out =  "CONGRUENTE";
		} else if (a % b != 0) {
			out =  "INCONGRUENTE";
		} else {
			out =  "ERROR";
		}
		return out;
	}

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		int a = 0, b = 0;
		System.out.print("a: ");
		a = s.nextInt();
		System.out.print("b: ");
		b = s.nextInt();
		System.out.println(ex1(a, b));
		s.close();
	}

}
