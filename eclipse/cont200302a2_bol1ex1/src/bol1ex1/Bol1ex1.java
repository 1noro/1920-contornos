package bol1ex1;

import java.util.Scanner;


public class Bol1ex1 {
	
	public static String ex1(int a, int b) {
		String out = "ERROR";
		
		try {
			if (a == b) {
				out = "IGUALES";
			} else if (a % b == 0) {
				out =  "CONGRUENTE";
			} else if (a % b != 0) {
				out =  "INCONGRUENTE";
			}
		} catch (Exception e) {}
		
		return out;
	}

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		String astr = "", bstr = "";
		int a = 0, b = 0;
		System.out.print("a: ");
		astr = s.nextLine();
		System.out.print("b: ");
		bstr = s.nextLine();

		try {
			a = Integer.parseInt(astr);
			b = Integer.parseInt(bstr);
			System.out.println(ex1(a, b));
		} catch (NumberFormatException nfe) {
			System.out.println("ERROR");
		};
		
		
		s.close();
	}

}
