# Pruebas de Validación

> TEMA 3, libro px 109-110

## Pruebas unitarias de caja negra

...

## Pruebas unitarias de caja blanca

...

## Pruebas de Aceptación

- Pruebas alfa: ...
- Pruebas beta: Usuario de confianza de confianza.

## Pruebas de Sistema

...

## Pruebas de integración

...

# Ejercicios

## Ejercicio 12

¿Qué es la validación? ¿Por qué deben incluirse los requisitos de validación en el Documento de Especificación De Requisitos?

> Porque el documento de especificación de requisitos es como si fuese un contrato y ahí es donde se presentan las condiciones de como se va a ser ese proyecto encargado.

> Porque el doc es como un contrato, ahi dice los requisitos, necesidades que tiene el cliente y que debemos satisfacer. Aparecerian las condiciones que pone el cliente para aceptar el producto
Pag 109, 110
Pruebas alfa,beta,aceptacion...
Pruebas de integracion
Pruebas de validacion: si la app cumple los requisitos del usuario, que estan escritos en el documento de val. Se hace mediante pruebas de caja negra, para comprobar hay que usar las mismas tecnicas pero con otro objetivo
solo existe el codigo final, se prueba el sistema completo, normalmente se hace uno o varios casos de prueba por cada requisito o cada caso de uso
Pruebas de aceptacion:

## Ejercicio 13

Dadas dos implementaciones alternativas de un algoritmo, ¿coincidirán las pruebas de caja negra que verifiquen el funcionamiento correcto de cada implementación? ¿Coincidirán las pruebas si son de caja transparente?

> - Si que coinciden.
> - No coincidirán.

> Si el programa sirve para lo mismo, deberian coincidir las pruebas de caja negra (estudian entradas y salidas)
La segunda no coincide, el codigo es diferente (algoritmo es distinto)

## Ejercicio 14

La agencia espacial europea está desarrollando un proyecto de un satélite artificial. El proyecto se realiza por partes en cuatro países: España, Francia, Inglaterra y Alemania. Al realizar ciertas pruebas se comprueba que el sistema tiene errores. ¿Qué tipo de pruebas serán éstas?

> Pruebas de integración.

> Pruebas unitarias de caja negra
Las pruebas que comprueban que errores tiene son: pruebas de integracion, hay distinto sistemas de unidades porque estan en la agencia espacial pero al parecer son tontos
